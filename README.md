Testing Utilities
=================

This module contains a number of utilities and helpers to help with testing.
It is based on the [junit 4](http://junit.org/junit4/) paradigms, but may be
updated to work with [junit 5](http://junit.org/junit5/) when that is out and
stable. It consists of a number of mostly independent parts.

See [docs/index.md](docs/index.md) for details.