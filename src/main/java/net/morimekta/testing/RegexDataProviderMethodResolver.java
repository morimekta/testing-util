package net.morimekta.testing;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import com.tngtech.java.junit.dataprovider.internal.DefaultDataProviderMethodResolver;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.TestClass;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Use a regex to match data provider method names instead of the default
 * name replacement strategy, or simple name matching. This allows for
 * depending on multiple data providers for the same test.
 *
 * <pre>{@code
 * {@literal@}UseDataProvider(value = "(getQueries|postQueries)",
 *                  resolveStrategy = AGGREGATE_ALL_MATCHES,
 *                  resolver = RegexDataProviderMethodResolver.class)
 * public void testPostGraphql(String query, String responseJson) throws IOException {
 *     // ...
 * }
 * }</pre>
 */
public class RegexDataProviderMethodResolver extends DefaultDataProviderMethodResolver {
    @Override
    protected List<FrameworkMethod> findDataProviderMethods(List<TestClass> locations, String testMethodName, String useDataProviderValue) {
        if (UseDataProvider.DEFAULT_VALUE.equals(useDataProviderValue)) {
            return super.findDataProviderMethods(locations, testMethodName, useDataProviderValue);
        }
        Pattern pattern = Pattern.compile(useDataProviderValue);
        List<FrameworkMethod> result = new ArrayList<>();
        for (TestClass location : locations) {
            result.addAll(findDataProviderMethodsByRegex(location, pattern));
        }
        return result;
    }

    private List<FrameworkMethod> findDataProviderMethodsByRegex(TestClass location, Pattern pattern) {
        List<FrameworkMethod> dataProviderMethods = location.getAnnotatedMethods(DataProvider.class);
        List<FrameworkMethod> result = new ArrayList<>();
        for (FrameworkMethod method : dataProviderMethods) {
            if (pattern.matcher(method.getName()).matches()) {
                result.add(method);
            }
        }
        return result;
    }
}
