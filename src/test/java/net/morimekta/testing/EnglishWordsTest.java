package net.morimekta.testing;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;

public class EnglishWordsTest {
    @Test
    public void testSentence() {
        for (int i = 25; i < 100; i += 5) {
            assertThat(EnglishWords.sentence(i + 1).length(), is(greaterThan(i)));
        }
    }

    @Test
    public void testParagraph() {
        String paragraph = EnglishWords.paragraph(3);
        String[] sentences = paragraph.split("[.]");
        assertThat(sentences.length, is(3));
    }
}
